#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by automation.
    The purpose of this script is to clean up the terraform deployment of a gke cluster.

    Arguments
        TFVARS_FILEPATH (Required) = The path to the tfvars file that will be used for testing.
        WORKSPACE_NAME (Required) = The name of the workspace to work with.
        BUCKET_NAME (Required) = The name of the workspace to work with.
END
)
echo "$HELPTEXT"
}

function error {
    echo $1
    echo
    echo "$(helptext)"
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    echo "$(helptext)" 
    exit 1
fi

TFVARS_FILEPATH=${1:?"$(error 'TFVARS_FILEPATH must be set' )"}
WORKSPACE_NAME=${2:?"$(error 'WORKSPACE_NAME must be set' )"}
BUCKET_NAME=${3:?"$(error 'BUCKET_NAME must be set' )"}

if ! [[ -e $TFVARS_FILEPATH ]] ; then
    error "$TFVARS_FILEPATH is not a file that exists."
fi

echo "GKE Cluster Clean Up Args:"
echo $TFVARS_FILEPATH
echo $WORKSPACE_NAME
echo $BUCKET_NAME

TFVARS_ABSOLUTEPATH_FILEPATH=$(realpath $TFVARS_FILEPATH)
pushd ${BASH_SOURCE%/*}/../terraform-gke/

sed -i "/bucket[[:space:]]*=/c\    bucket  = \"$BUCKET_NAME\"" main.tf
terraform init
terraform workspace select $WORKSPACE_NAME
terraform destroy -var-file=$TFVARS_ABSOLUTEPATH_FILEPATH -auto-approve
terraform workspace select default
terraform workspace delete $WORKSPACE_NAME
popd

gsutil rm -r gs://$BUCKET_NAME
