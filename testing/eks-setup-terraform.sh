#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by automation.
    The purpose of this script is to set up the terraform deployment of an eks cluster.

    Arguments
        UNIQUE_IDENTIFIER (Required) = A unique identifier that will be used to create with this script.
END
)
echo "$HELPTEXT"
}

function error {
    echo $1
    echo
    echo "$(helptext)"
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    echo "$(helptext)" 
    exit 1
fi

UNIQUE_IDENTIFIER=${1:?"$(error 'UNIQUE_IDENTIFIER must be set' )"}

CLUSTER_NAME="xand-devops-testing-$UNIQUE_IDENTIFIER"

BUCKET_NAME="xand-devops-testing-bucket-$UNIQUE_IDENTIFIER"
REGION="us-west-2"
EC2_KEY_PAIR_NAME="xand-ssh"

cp ${BASH_SOURCE%/*}/../terraform-eks/example.tfvars ${BASH_SOURCE%/*}/xand-devops-eks.tfvars
TFVARS_ABSOLUTEPATH_FILEPATH=$(realpath ${BASH_SOURCE%/*}/xand-devops-eks.tfvars)

sed -e "s|{CLUSTER_NAME}|${CLUSTER_NAME}|g" \
    -i $TFVARS_ABSOLUTEPATH_FILEPATH

pushd ${BASH_SOURCE%/*}/../terraform-eks/
echo "Cluster Name: $CLUSTER_NAME"
echo "Storage Container Name: $BUCKET_NAME"
./scripts/create-aws-storage.sh "$BUCKET_NAME" "$REGION"
./scripts/init_tf_env.sh "$CLUSTER_NAME"
terraform workspace list

XAND_SSH_KEY_PAIR_COUNT=$(aws ec2 describe-key-pairs \
    --filters "Name=key-name,Values=$EC2_KEY_PAIR_NAME" \
    --region "$REGION" \
      | jq '.KeyPairs | length')
if [ $XAND_SSH_KEY_PAIR_COUNT -eq 0 ]; then
    aws ec2 create-key-pair --key-name "$EC2_KEY_PAIR_NAME" --region "$REGION"
else
    echo "Bypassed creation of AWS EC2 key-pair: \"$EC2_KEY_PAIR_NAME\", as it already exists."
fi


popd
