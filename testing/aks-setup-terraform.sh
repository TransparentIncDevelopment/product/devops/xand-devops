#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by automation.
    The purpose of this script is to set up for a terraform deployment of an aks cluster. It assumes the `az` CLI is logged in.

    Arguments
        UNIQUE_IDENTIFIER (Required) = A unique identifier that will be used to create with this script.
END
)
echo "$HELPTEXT"
}

function error {
    echo $1
    echo
    echo "$(helptext)"
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    echo "$(helptext)"
    exit 1
fi

# Check input args
UNIQUE_IDENTIFIER=${1:?"$(error 'UNIQUE_IDENTIFIER must be set' )"}

# Build vars for naming uniquely identifiable resources
RESOURCE_GROUP="rg-$UNIQUE_IDENTIFIER"
REGION="westus2"
# Trim uniqueidentifier to match storage account requirements (lower case and numbers only)
STORAGE_ACCOUNT=$(echo "xandstorage$UNIQUE_IDENTIFIER" | tr -cd '[a-z0-9]')
STORAGE_CONTAINER="container-$UNIQUE_IDENTIFIER"
T4_WORKSPACE="workspace-$UNIQUE_IDENTIFIER"


# Print vars for debugging purposes
echo "resource group: $RESOURCE_GROUP"
echo "region: $REGION"
echo "storage account: $STORAGE_ACCOUNT"
echo "storage container: $STORAGE_CONTAINER"
echo "terraform workspace: $T4_WORKSPACE"

pushd ${BASH_SOURCE%/*}/../terraform-aks/

# Set up storage account and init terraform env
./scripts/create-azure-storage.sh "$RESOURCE_GROUP" "$STORAGE_ACCOUNT" "$STORAGE_CONTAINER" "$REGION"
./scripts/configure-terraform-backend.sh "$RESOURCE_GROUP" "$STORAGE_ACCOUNT" "$STORAGE_CONTAINER"
./scripts/init_tf_env.sh "$T4_WORKSPACE"

# Print terraform workspaces for debugging purposes
terraform workspace list

popd
