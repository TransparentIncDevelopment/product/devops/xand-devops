#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by automation.
    The purpose of this script is to test the terraform deployment of a gke cluster.

    Arguments
        TFVARS_FILEPATH (Required) = The path to the tfvars file that will be used for testing.
        WORKSPACE_NAME (Required) = The name of the workspace to work with.
END
)
echo "$HELPTEXT"
}

function error {
    echo $1
    echo
    echo "$(helptext)"
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    echo "$(helptext)" 
    exit 1
fi

TFVARS_FILEPATH=${1:?"$(error 'TFVARS_FILEPATH must be set' )"}
WORKSPACE_NAME=${2:?"$(error 'WORKSPACE_NAME must be set' )"}

if ! [[ -e $TFVARS_FILEPATH ]] ; then
    error "$TFVARS_FILEPATH is not a file that exists."
fi

echo "GKE Cluster Deployment Args:"
echo $TFVARS_FILEPATH
echo $WORKSPACE_NAME

TFVARS_ABSOLUTEPATH_FILEPATH=$(realpath $TFVARS_FILEPATH)
pushd ${BASH_SOURCE%/*}/../terraform-gke/
./scripts/init_tf_env.sh $WORKSPACE_NAME

terraform workspace list
terraform apply -var-file=$TFVARS_ABSOLUTEPATH_FILEPATH -auto-approve
popd
