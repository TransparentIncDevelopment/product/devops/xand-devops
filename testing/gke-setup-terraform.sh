#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by automation.
    The purpose of this script is to set up the terraform deployment of a gke cluster.

    Arguments
        UNIQUE_IDENTIFIER (Required) = A unique identifier that will be used to create with this script.
END
)
echo "$HELPTEXT"
}

function error {
    echo $1
    echo
    echo "$(helptext)"
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    echo "$(helptext)" 
    exit 1
fi

UNIQUE_IDENTIFIER=${1:?"$(error 'UNIQUE_IDENTIFIER must be set' )"}

GCP_PROJECT="xand-qa"
GCP_REGION="us-west4"
GCP_ZONE="us-west4-c"

CLUSTER_NAME="xand-devops-testing-$UNIQUE_IDENTIFIER"

BUCKET_NAME="xand-devops-testing-bucket-$UNIQUE_IDENTIFIER"

cp ${BASH_SOURCE%/*}/../terraform-gke/example.tfvars ${BASH_SOURCE%/*}/xand-devops-gke.tfvars
TFVARS_ABSOLUTEPATH_FILEPATH=$(realpath ${BASH_SOURCE%/*}/xand-devops-gke.tfvars)

sed -e "s|{GCP_PROJECT}|${GCP_PROJECT}|g" \
    -e "s|{GCP_REGION}|${GCP_REGION}|g" \
    -e "s|{GCP_ZONE}|${GCP_ZONE}|g" \
    -e "s|{CLUSTER_NAME}|${CLUSTER_NAME}|g" \
    -i $TFVARS_ABSOLUTEPATH_FILEPATH

pushd ${BASH_SOURCE%/*}/../terraform-gke/
echo "Cluster Name: $CLUSTER_NAME"
echo "Bucket Name: $BUCKET_NAME"
./scripts/create_tf_shared_state.sh $BUCKET_NAME
popd
