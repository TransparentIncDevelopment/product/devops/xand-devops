#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by automation.
    The purpose of this script is to set up for a terraform deployment of an aks cluster. It assumes the `az` CLI is logged in.

    Arguments
        UNIQUE_IDENTIFIER (Required) = A unique identifier that will be used to create with this script.
END
)
echo "$HELPTEXT"
}

function error {
    echo $1
    echo
    echo "$(helptext)"
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    echo "$(helptext)"
    exit 1
fi

# Check input args
UNIQUE_IDENTIFIER=${1:?"$(error 'UNIQUE_IDENTIFIER must be set' )"}

# Build vars for naming uniquely identifiable resources
RESOURCE_GROUP="rg-$UNIQUE_IDENTIFIER"
CLUSTER_NAME="cluster-$UNIQUE_IDENTIFIER"

# Print vars for debugging purposes
echo "cluster_name: $CLUSTER_NAME"

pushd ${BASH_SOURCE%/*}/../terraform-aks/

echo "Running terraform destroy"
# Destroy cluster
terraform destroy \
  -var-file=example-prod.tfvars \
  -var cluster_name="$CLUSTER_NAME" \
  -auto-approve

# Delete the resource group containing the storage account
echo "Deleting resource group: $RESOURCE_GROUP"
az group delete --name "$RESOURCE_GROUP" --yes

popd
