![logo](img/devops_cycle.png)

# Xand Dev Ops

This repo contains our dev ops tools for automating :

- New env build-outs (Cloud, local etc.)
    - [K8s as a service : EKS vs. AKS vs. GKE](https://blog.alcide.io/kubernetes-as-a-service-eks-vs.-aks-vs.-gke)
- Auditing (Security & resource usage)
- Metrics systems
- more
