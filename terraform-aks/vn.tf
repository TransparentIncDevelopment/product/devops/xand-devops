resource "azurerm_resource_group" "xand" {
  name     = "xand-${var.cluster_name}"
  location = var.azure_region
}

resource "azurerm_virtual_network" "xand" {
  name                = "xand-network"
  address_space       = ["10.168.0.0/16"]
  resource_group_name = azurerm_resource_group.xand.name
  location            = azurerm_resource_group.xand.location
}

# Public IP and NAT closely follow terraform documentation
# Reference: https://www.terraform.io/docs/providers/azurerm/r/nat_gateway.html

resource "azurerm_public_ip" "xand_nat_ip" {
  name                = "xand-nat-ip"
  location            = azurerm_resource_group.xand.location
  resource_group_name = azurerm_resource_group.xand.name
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_nat_gateway" "xand_nat" {
  name                = "xand-nat"
  location            = azurerm_resource_group.xand.location
  resource_group_name = azurerm_resource_group.xand.name
  public_ip_address_ids = [azurerm_public_ip.xand_nat_ip.id]
}

resource "azurerm_subnet" "xand_private_subnet" {
  name                 = "xand-private-subnet"
  resource_group_name  = azurerm_resource_group.xand.name
  virtual_network_name = azurerm_virtual_network.xand.name
  address_prefix       = "10.168.0.0/20"
}

resource "azurerm_subnet_nat_gateway_association" "example" {
  subnet_id      = azurerm_subnet.xand_private_subnet.id
  nat_gateway_id = azurerm_nat_gateway.xand_nat.id
}

# Reference: https://github.com/terraform-providers/terraform-provider-azuread/issues/40#issuecomment-491015634
resource "azurerm_role_assignment" "subnet_network_contributer" {
  scope              = azurerm_subnet.xand_private_subnet.id
  role_definition_name = "Network Contributor"
  principal_id       = local.service_principal_object_id
}

resource "azurerm_role_assignment" "rg_contributor" {
  scope              = azurerm_resource_group.xand.id
  role_definition_name = "Contributor"
  principal_id       = local.service_principal_object_id
}
