![t4 logo](../img/t4.png) ![aks logo](../img/aks.png) ![k8s logo](../img/k8s.png) ![tpfs logo](../img/tpfs.png)

# Terraform AKS

This will set up an auto repairing, upgrading, scaling [AKS (Azure Kubernetes Service)](https://docs.microsoft.com/en-us/azure/aks/intro-kubernetes) cluster using [Terraform](https://www.terraform.io/).

## pre-requisites

- [Install Terraform](https://www.terraform.io/downloads.html)
- [Install kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [Install Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli)

Alternatively, see [`../docker/README.md`](../docker/README.md) for instructions to work within a docker
container which has all required dependencies.

## Refs
- https://docs.microsoft.com/en-us/azure/terraform/terraform-create-k8s-cluster-with-tf-and-aks

## Set up Terraform Backend Storage (Azure)

Azure storage can be used to back Terraform [workspaces](https://www.terraform.io/docs/state/workspaces.html).
A Terraform workspace captures the state for a set of resources managed together.

You can create your own Azure storage account and container to back Terraform workspaces, or reference
an existing storage account and container your organization may already be using for shared workspaces.

If you are a TPFS employee you may skip the next section. There is already a shared blob container for
our Terraform workspaces.

### 3rd Parties Only: Create new Azure Storage Account and Container

Run the following if you need to create a new Storage Account and Container to act as a Terraform backend.

Set variables for creating a storage account and container and run the script as shown to provision the
resources in Azure.
```bash
export RESOURCE_GROUP="<resource_group_name>"    # Example "tpfs-dev-rg"
export STORAGE_ACCOUNT="<storage_account_name>"  # Example "tpfsdevstorage"
export STORAGE_CONTAINER="terraform-container"   # Optional argument
export REGION="westus"                           # Optional argument

./scripts/create-azure-storage.sh "$RESOURCE_GROUP" "$STORAGE_ACCOUNT" "$STORAGE_CONTAINER" "$REGION"
```

### TPFS Employees Only: Shared Azure Storage

Set the following environment variables:

```
export RESOURCE_GROUP=tpfs-dev-rg
export STORAGE_ACCOUNT=tpfsdevstorage
export STORAGE_CONTAINER=terraform-container
``` 

## Configure the Terraform backend

Run the following script to populate the `main-backend.tf` file.

```bash
./scripts/configure-terraform-backend.sh "$RESOURCE_GROUP" "$STORAGE_ACCOUNT" "$STORAGE_CONTAINER"
```

## Set up Terraform Workspace

With the Terraform backend configured, run `./scripts/init_tf_env.sh {MY_WORKSPACE_NAME}` script to

- initialize any plugins required
- connect to the configured blob storage
- initialize the Terraform workspace if it doesn't exist

For example:
```bash
./scripts/init_tf_env.sh "${USER}-prod"
```

initializes a workspace named as your username suffixed by "-prod".

See existing workspaces tracked in the configured backend with:
```bash
terraform workspace list
```

## Set up Terraform variables files (`.tfvars`)

Personal environments allow you to name your own cluster for each stage and specify resources.

Azure requires an ssh cert for configuring nodes within the K8s cluster. Generate one by following
the prompts and setting an empty password:

```bash
mkdir -p ~/xand/ssh
ssh-keygen -b 4096 -f ~/xand/ssh/id_rsa
```

See an example personal prod environment configuration at `example-prod.tfvars`. To make your own,
copy it to `${USER}-prod.tfvars` and then update its variables. TPFS should provide you the Artifactory details.

```bash
cp "example-prod.tfvars" "${USER}-prod.tfvars"
$EDITOR "${USER}-prod.tfvars"
```

If your ssh cert was generated in a different location, uncomment `ssh_public_key` below and set the correct path.

All assigned values between `{` and `}` should be replaced, including the braces themselves. For example,
`cluster_name = "{cluster}"` may become `cluster_name = "mycluster"`.

## Deploy a new cluster

Now that Terraform has been properly configured go ahead and tell it to apply the configuration and launch the cluster.

```
terraform apply -var-file="${USER}-prod.tfvars"
```
