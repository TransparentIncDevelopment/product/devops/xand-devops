#!/bin/bash

set -o errexit # abort on nonzero exit status
set -o nounset # abort on unbound variable
set -o pipefail # abort if any element of a pipeline fails

TERRAFORM_WORKSPACE=$1

if [ -z "$TERRAFORM_WORKSPACE" ]
then
  echo "You must specify the workspace name e.g. 'companyname-dev', 'companyname-test', 'companyname-qa', or 'companyname-prod' etc."
  exit 1
fi

echo "Terraform Environment : $TERRAFORM_WORKSPACE"

terraform init
terraform workspace new ${TERRAFORM_WORKSPACE} || true
terraform workspace select ${TERRAFORM_WORKSPACE}
