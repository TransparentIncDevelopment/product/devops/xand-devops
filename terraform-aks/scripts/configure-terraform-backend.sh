#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

### When blob storage is not set up, "create-azure-storage.sh" should be run first

function helptext () {
    local text=$(cat <<EOF
    Configure Terraform to use an existing Azure blob storage container for workspace state

    Arguments
        RESOURCE_GROUP (Required) = Azure resource group for the blob container.
        STORAGE_ACCOUNT (Required) = Azure storage account name to be provisioned in RESOURCE_GROUP.
        STORAGE_CONTAINER (Required) = Azure blob container name.
EOF
	  )
    echo "$text"
}

function error () {
    echo "$@"
    echo
    echo "$(helptext)"
    exit 1
}

function info () {
    local green="\033[32m"
    local normal="\033[00m"
    echo -e "${green}$1${normal}"
}

if [[ $# -lt 3 ]]; then
    error "Not enough arguments provided"
fi

RESOURCE_GROUP=${1:?"$(error 'RESOURCE_GROUP must be set')"}
STORAGE_ACCOUNT=${2:?"$(error 'STORAGE_ACCOUNT must be set')"}
STORAGE_CONTAINER=${3:?"$(error 'STORAGE_CONTAINER must be set')"}

SRC_DIR="$(dirname "$(readlink -f "$0")")"
MAIN_BACKEND="main-backend.tf"

if [[ $(az account list | jq length) -eq 0 ]]; then
    info "Logging into Azure ..."
    az login
else
    info "Already logged into Azure"
fi

info "Fetching access keys for the ${STORAGE_ACCOUNT} storage account ..."
STORAGE_ACCESS_KEY=$(az storage account keys list --account-name "$STORAGE_ACCOUNT" --resource-group "$RESOURCE_GROUP" | jq -r ".[0].value")
info "Using access key: ${STORAGE_ACCESS_KEY}"

info "Creating '${MAIN_BACKEND}' file to configure the Terraform backend for Azure ..."
tee "${SRC_DIR}/../$MAIN_BACKEND" <<EOF
terraform {
    backend "azurerm" {
      storage_account_name = "${STORAGE_ACCOUNT}"
      container_name       = "${STORAGE_CONTAINER}"
      key                  = "xand"
      access_key           = "${STORAGE_ACCESS_KEY}"
    }
}
EOF
