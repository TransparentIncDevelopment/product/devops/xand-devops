#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

function helptext () {
    local text=$(cat <<EOF
    Set up Azure blob storage container for Terraform workspace

    Arguments
        RESOURCE_GROUP (Required) = Azure resource group for the blob container.
        STORAGE_ACCOUNT (Required) = Azure storage account name to be provisioned in RESOURCE_GROUP.
        STORAGE_CONTAINER (Optional) = Azure blob container name. Defaults to 'terraform-container'.
      	REGION (Optional) = Azure region for the blob storage. Defaults to 'westus2'.
EOF
	  )
    echo "$text"
}

function error () {
    echo "$@"
    echo
    echo "$(helptext)"
    exit 1
}

function info () {
    local green="\033[32m"
    local normal="\033[00m"
    echo -e "${green}$1${normal}"
}

if [[ $# -lt 2 ]]; then
    error "Not enough arguments provided"
fi

RESOURCE_GROUP=${1:?"$(error 'RESOURCE_GROUP must be set')"}
STORAGE_ACCOUNT=${2:?"$(error 'STORAGE_ACCOUNT must be set')"}
STORAGE_CONTAINER=${3:-"terraform-container"}
REGION=${4:-"westus2"}

if [[ $(az account list | jq length) -eq 0 ]]; then
    info "Logging into Azure ..."
    az login
else
    info "Already logged into Azure"
fi

info "Creating an Azure resource group called '${RESOURCE_GROUP}' in the ${REGION} region ..."
az group create \
    --name "$RESOURCE_GROUP" \
    --location "$REGION"

info "Creating a storage account called '${STORAGE_ACCOUNT}' within the ${RESOURCE_GROUP} resource group ..."
az storage account create \
    --name "$STORAGE_ACCOUNT" \
    -g "$RESOURCE_GROUP"

info "Creating a blob container called '${STORAGE_CONTAINER}' within the ${STORAGE_ACCOUNT} storage account ..."
az storage container create \
    --auth-mode login \
    --account-name "$STORAGE_ACCOUNT" \
    --name "$STORAGE_CONTAINER"

info "Successfully provisioned a blob container for Terraform!"

### Configuration of bucket and terraform continues in xand-devops/terraform-aks/scripts/configure-terraform-backend.sh
