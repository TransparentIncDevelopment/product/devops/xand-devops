variable "ad_app_name" {
  type = string
  description = "Name for Active Directory enterprise application. The service principal associated with the AKS cluster will be created within this AD application"
}

variable "cluster_name" {
  type        = string
  description = "The K8S cluster name"
}

variable "cluster_version" {
  type        = string
  default     = "1.22"
  description = "The K8S cluster version"
}

variable "azure_region" {
  type        = string
  description = "Default Azure region"
}

variable "namespace_name" {
  default = "xand-service"
  type    = string
}

variable "subnet_count" {
  default = 2
}

variable "azure_vm_size" {
  default = "Standard_D8_v3"
  type = string
}

variable "azure_node_min_count" {
  default = 2
}

variable "azure_node_max_count" {
  default = 15
}

variable "ssh_public_key" {
  default = "~/xand/ssh/id_rsa.pub"
}

variable "service_principal_application_id" {
  type        = string
  description = "Service Principal Application Id if one is already created"
  default     = null
}

variable "service_principal_secret" {
  type        = string
  description = "Service Principal Client Secret if one is already created"
  default     = null
}
