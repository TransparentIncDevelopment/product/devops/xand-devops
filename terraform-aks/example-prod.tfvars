# Active Directory app name (to act as serivce principal's context)
ad_app_name = "aks-ad-app"

# AKS settings
azure_region = "westus2"

// Uncomment to optionally set explicity path to an ssh public key file instead of default
//ssh_public_key = "~/xand/ssh/id_rsa.pub"

azure_node_min_count = 2
azure_node_max_count = 4
azure_vm_size = "Standard_D8_v3"

cluster_name = "{CLUSTER_NAME}"

# Ansible Service Principal Route
service_principal_application_id=null
service_principal_secret=null
