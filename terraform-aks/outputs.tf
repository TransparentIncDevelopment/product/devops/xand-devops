#
# Outputs
#

output "ad_service_principal_id" {
    value = azuread_service_principal.k8s_ad_service_principal[*].application_id
}

output "ad_service_principal_display_name" {
    value = azuread_service_principal.k8s_ad_service_principal[*].display_name
}

output "client_key" {
    value = azurerm_kubernetes_cluster.xand.kube_config.0.client_key
    sensitive = true
}

output "client_certificate" {
    value = azurerm_kubernetes_cluster.xand.kube_config.0.client_certificate
    sensitive = true
}

output "cluster_ca_certificate" {
    value = azurerm_kubernetes_cluster.xand.kube_config.0.cluster_ca_certificate
    sensitive = true
}

output "cluster_username" {
    value = azurerm_kubernetes_cluster.xand.kube_config.0.username
}

output "cluster_password" {
    value = azurerm_kubernetes_cluster.xand.kube_config.0.password
    sensitive = true
}

output "kube_config" {
    value = azurerm_kubernetes_cluster.xand.kube_config_raw
    sensitive = true
}

output "host" {
    description = "The address that can be used to connect to for kubectl"
    value = azurerm_kubernetes_cluster.xand.kube_config.0.host
}

output "nat_ips" {
    description = "The IP addresses outgoing traffic will be associated with"
    value = [azurerm_public_ip.xand_nat_ip.ip_address]
}

output "network_profile" {
    value = azurerm_kubernetes_cluster.xand.network_profile
}
