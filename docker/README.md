# Docker
This `xand-devops` repo contains the definitions and scripts to deploy a K8s clusters with the 
default prescribed services (telemetry, monitoring, etc.) to each of the supported clouds.

The `/docker/Dockerfile` definition produces a docker image that contains the dependencies 
needed(`terraform`, `az`, `aws`, etc.) to use those definitions and scripts.

The `Makefile` in this directory helps manage the docker image and interactively 
work with a running instance of it.

## Dependencies

You must have docker engine above ver `18.09`, and you must pass ARTIFACTORY_USER and
ARTIFACTORY_PASS build args (`--build-arg`) to the docker container when building. This
will be handled for you if you have them set as environment variables when calling make.


## Typical workflows

From this `./docker` directory, run 
```bash
make build
```

Start a session in a container
```bash
make run
```

Do some stuff, see project source mounted at `/xand-devops`
```
root@7d02ab3d62fc:/# cd /xand-devops
root@7d02ab3d62fc:/# az login 
```

Exit
```
root@7d02ab3d62fc:/# exit
```

Re-enter (attach) to your container. 
State will persist through exiting and attaching to the container
```
make attach
```

Kill and remove the container (and state) with 
```
make kill
```

## Image Publishing

Do not publish your local image unless you are specifically testing something related to docker image publishing. In general let
our CI pipeline take care of publishing. The Makefile is designed to enable semantic versioning during the CI pipeline.

## Commands

### print

`make print` outputs the variables that will be used for other commands in the makefile.

### build

`make build` build the `xand-devops-toolchain` image from the local Dockerfile

### check-version (not yet implemented)

`make check-version` will verify that the container does _not_ exist in the registry. This target
will fail if the container already exists. 

### publish

`make publish` will publish the container to the registry. It runs `check-version` to protect against
overwriting containers.

### run

`make run` will initialize and run the locally produced container.

### start

`make start` will re-start a previously exited or stopped container

### attach

`make attach` will start and attach a previously exited or stopped container 

### kill

`make kill` will kill the locally produced container.
