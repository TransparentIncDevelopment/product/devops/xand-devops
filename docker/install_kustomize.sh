#!/bin/bash

KUSTOMIZE_URL="https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv3.5.5/kustomize_v3.5.5_linux_amd64.tar.gz"

curl -Lo /tmp/kustomize.tgz $KUSTOMIZE_URL
tar xvf /tmp/kustomize.tgz
chmod +x kustomize
mv kustomize /usr/local/bin/
