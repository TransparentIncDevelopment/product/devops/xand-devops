#!/bin/bash

# Installs our own rust tool for generating keys for xand nodes.
curl --fail -u${ARTIFACTORY_USER}:${ARTIFACTORY_PASS} -LO https://transparentinc.jfrog.io/artifactory/artifacts-internal/xkeygen/xkeygen_1.1.1_amd64.deb
dpkg -i xkeygen_1.1.1_amd64.deb
