#!/bin/bash

KUBECTL_URL="https://storage.googleapis.com/kubernetes-release/release/v1.24.3/bin/linux/amd64/kubectl"

curl -Lo /usr/local/bin/kubectl $KUBECTL_URL
chmod +x /usr/local/bin/kubectl
