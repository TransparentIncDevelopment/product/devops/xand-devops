#!/bin/bash

# Installs our own rust tool for generating JWKS and JWTs
curl --fail -u${ARTIFACTORY_USER}:${ARTIFACTORY_PASS} -LO https://transparentinc.jfrog.io/artifactory/artifacts-internal/jwtcli/jwtcli_0.4.1_amd64.deb
dpkg -i jwtcli_0.4.1_amd64.deb
