variable "gcp_project" {
  type        = string
  description = "Google Cloud project name"
}

variable "cluster_name" {
  type        = string
  description = "The K8S cluster name"
}

variable "cluster_version" {
  type        = string
  default     = "1.22"
  description = "The K8S cluster version"
}

variable "gcp_region" {
  type        = string
  description = "Default Google Cloud Region"
}

variable "gcp_zone" {
  type        = string
  description = "Default Google Cloud Zone"
}

variable "gcp_machine_type" {
  type        = string
  description = "Machine type to use for the general-purpose node pool. See https://cloud.google.com/compute/docs/machine-types"
  default = "n1-standard-8"
}

variable "gcp_min_node_count" {
  type        = string
  description = "The minimum number of nodes PER ZONE in the general-purpose node pool"
  default     = 2
}

variable "gcp_max_node_count" {
  type        = string
  description = "The maximum number of nodes PER ZONE in the general-purpose node pool"
  default     = 6
}

variable "cluster_username" {
  type        = string
  description = "The GKE cluster username"
  default     = "admin"
}

variable "cluster_password" {
  type        = string
  description = "The GKE cluster password"
}
