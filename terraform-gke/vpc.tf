resource "google_compute_network" "xand" {
  project = var.gcp_project
  name = "${var.cluster_name}-vpc-network"
  auto_create_subnetworks = false
  routing_mode  = "REGIONAL"
}

resource "google_compute_subnetwork" "xand" {
  project = var.gcp_project
  name = "${var.cluster_name}-private-subnet"
  network       = google_compute_network.xand.self_link
  ip_cidr_range = "192.168.0.0/24"
  region        = var.gcp_region
}
