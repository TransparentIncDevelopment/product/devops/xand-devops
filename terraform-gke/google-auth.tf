data "google_client_openid_userinfo" "current_userinfo" {
}

resource "local_file" "google_auth" {
  depends_on = [
    google_container_cluster.xand,
  ]

  content = templatefile("${path.module}/google-auth.tpl", {
    name = data.google_client_openid_userinfo.current_userinfo.email
  })
  filename = "${path.module}/google-auth.yaml"
}

resource "null_resource" "update_kubeconfig" {
  depends_on = [
    google_container_cluster.xand
  ]

  # Always run in case the .kube/config file doesn't exist locally.
  triggers = {
    always_run = "${timestamp()}"
  }

  provisioner "local-exec" {
    command = "gcloud container clusters get-credentials ${var.cluster_name} --zone ${var.gcp_zone}"
  }

  provisioner "local-exec" {
    command = "kubectl apply -f google-auth.yaml && rm google-auth.yaml"
  }
}
