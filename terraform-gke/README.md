![t4 logo](../img/t4.png) ![gke logo](../img/gke.png) ![k8s logo](../img/k8s.png) ![tpfs logo](../img/tpfs.png)

# Terraform GKE
This will set up an auto repairing, upgrading, scaling [GKE (Google K8s Engine)](https://cloud.google.com/kubernetes-engine) cluster using [Terraform](https://www.terraform.io/) with [Google Storage](https://cloud.google.com/storage) as the [Terraform Remote Storage](https://www.terraform.io/docs/state/remote.html) backend.

## Background

The [Xand Platform Runbook](https://transparentinc.atlassian.net/wiki/spaces/XNS/pages/259915781/Xand+Platform+Runbook) has background context on why this exists and how it fits into our infrastructure.

## pre-requisites

- [Install Terraform](https://www.terraform.io/downloads.html)
- [Install kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [Install the Google Cloud SDK](https://cloud.google.com/sdk/install)
- Clone this repo locally

## Login to Google Cloud

gcloud auth login

## Workspace Environments

Terraform uses the concept of a [workspace](https://www.terraform.io/docs/state/workspaces.html) for managing state. We use a workspace to define multiple environments whether shared or personal. For example shared workspace environments could contain `dev`, `test`, `qa` or `prod` stages for a shared Google project such as `xand-dev` or `xand-prod`. Let's not worry about this for now. They will be covered elsewhere in the Xand Platform runbooks.

Personal environments allow you to name your own cluster for each stage and specify resources.
Fill out `your_user_name-dev.tfvars` by copying example.tfvars file for configuration replacement values.

Create the file in the repo root directory and be sure to change the `cluster_name`.

## Set up Terraform cloud state & environment

Run the `init_tf_env.sh` script to create a unique Cloud Storage bucket and initialize the TerraForm personal workspace environments.

```console
|jgrant| mephisto in ~/projects/xand-devops/terraform-gke
[4826_4827_GKE_VPC_NAT ⚠★] → scripts/init_tf_env.sh jgrant-dev
TerraForm Environment : jgrant-dev
Initializing modules...

Initializing the backend...

Initializing provider plugins...

The following providers do not have any version constraints in configuration,
so the latest version was installed.

To prevent automatic upgrades to new major versions that may contain breaking
changes, it is recommended to add version = "..." constraints to the
corresponding provider blocks in configuration, with the constraint strings
suggested below.

* provider.google: version = "~> 3.21"
* provider.null: version = "~> 2.1"

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
Created and switched to workspace "jgrant-dev"!

You're now on a new, empty workspace. Workspaces isolate their state,
so if you run "terraform plan" Terraform will not see any existing state
for this configuration.

|jgrant| mephisto in ~/projects/xand-devops/terraform-gke
[4826_4827_GKE_VPC_NAT ⚠★] →
```

## Deploy a new cluster

It's really easy now to just spin up a new k8s GKE cluster for the `dev` env.

The output should look something like this ...

```console
|jgrant| kraken in ~/projects/xand-devops/terraform-gke
[3293-t4-k8s-sidecars ⚠] → terraform apply -var-file=jgrant-dev.tfvars
```


## Destroy an existing cluster

```console

|jgrant| kraken in ~/projects/xand-devops/terraform-gke
[3293-t4-k8s-sidecars ✔] → terraform destroy -var-file=jgrant-dev.tfvars -auto-approve
```
