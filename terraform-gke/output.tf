# The following outputs allow authentication and connectivity to the GKE Cluster
# by using certificate-based authentication.

output "cluster_full_name" {
  value = google_container_cluster.xand.name
}

output "cluster_endpoint_uri" {
  description = "The address that can be used to connect to for kubectl"
  value = google_container_cluster.xand.endpoint
}

output "cluster_username" {
  value = google_container_cluster.xand.master_auth.0.username
}

output "cluster_password" {
  value = google_container_cluster.xand.master_auth.0.password
  sensitive = true
}

output "client_certificate" {
  value = google_container_cluster.xand.master_auth.0.client_certificate
  sensitive = true
}

output "client_key" {
  value = google_container_cluster.xand.master_auth.0.client_key
  sensitive = true
}

output "cluster_ca_certificate" {
  value = google_container_cluster.xand.master_auth.0.cluster_ca_certificate
  sensitive = true
}

output "nat_ips" {
  description = "The IP addresses outgoing traffic will be associated with"
  value = google_compute_address.xand_nat.*.address
}
