provider "google" {
  project     = var.gcp_project
  region      = var.gcp_region
  zone        = var.gcp_zone
}

terraform {
  backend "gcs" {
    # The bucket name is set by the init script as vars are not allowed
    # in Terraform backend configs.
    bucket  = "xand-devops-testing-bucket-162235157"
    prefix  = "terraform"
  }
}
