data "google_container_engine_versions" "container_engine_version" {
  location = var.gcp_zone
  version_prefix = "${var.cluster_version}."
}

resource "google_container_cluster" "xand" {
  project  = var.gcp_project
  name     = var.cluster_name
  location = var.gcp_zone

  min_master_version = data.google_container_engine_versions.container_engine_version.latest_master_version

  network = google_compute_network.xand.self_link
  subnetwork = google_compute_subnetwork.xand.self_link

  # Setting an empty username and password explicitly disables basic auth
  master_auth {
    username = var.cluster_username
    password = var.cluster_password
  }

  # Add a cluster label to resources
  resource_labels = {
    xand_cluster_name = var.cluster_name
    service = "xand"
  }

  # https://www.terraform.io/docs/providers/google/r/container_cluster.html#private_cluster_config
  private_cluster_config {
    enable_private_endpoint = false
    enable_private_nodes = true
    master_ipv4_cidr_block = "192.168.1.0/28"
  }

  # Required for private VPC node cluster.
  # This enables IP aliasing.
  ip_allocation_policy { }

  node_pool {
    name     = var.cluster_name
    node_count = var.gcp_min_node_count

    management {
      auto_repair  = "true"
      auto_upgrade = "true"
    }

    autoscaling {
      min_node_count = var.gcp_min_node_count
      max_node_count = var.gcp_max_node_count
    }

    node_config {
      preemptible  = false

      machine_type = var.gcp_machine_type

      metadata = {
        disable-legacy-endpoints = "true"
      }

      # Needed for correctly functioning cluster, see
      # https://www.terraform.io/docs/providers/google/r/container_cluster.html#oauth_scopes
      oauth_scopes = [
        "https://www.googleapis.com/auth/logging.write",
        "https://www.googleapis.com/auth/monitoring",
        "https://www.googleapis.com/auth/devstorage.read_only"
      ]
    }
  }
}
