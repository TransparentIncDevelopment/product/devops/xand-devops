variable "octopus_address" {
  default = "https://my-octopus-server"
}
variable "octopus_apikey" {
  default = "API-YOURAPIKEYGOESHERE"
}
variable "octopus_space" {
  default = "Default"
}
