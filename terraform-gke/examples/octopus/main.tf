provider "octopusdeploy" {
  address = var.octopus_address
  apikey  = var.octopus_apikey
  space   = var.octopus_space
}

# Examples usage ...
# https://github.com/OctopusDeploy/terraform-provider-octopusdeploy/blob/master/examples/main.tf
