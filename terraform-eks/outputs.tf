#
# Outputs
#

locals {
  config-map-aws-auth = <<CONFIGMAPAWSAUTH


apiVersion: v1
kind: ConfigMap
metadata:
  name: aws-auth
  namespace: kube-system
data:
  mapRoles: |
    - rolearn: ${aws_iam_role.cluster_node.arn}
      username: system:node:{{EC2PrivateDNSName}}
      groups:
        - system:bootstrappers
        - system:nodes
CONFIGMAPAWSAUTH

  kubeconfig = <<KUBECONFIG


apiVersion: v1
clusters:
- cluster:
    server: ${aws_eks_cluster.xand.endpoint}
    certificate-authority-data: ${aws_eks_cluster.xand.certificate_authority.0.data}
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: aws
  name: aws
current-context: aws
kind: Config
preferences: {}
users:
- name: aws
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      command: ../bin/aws-iam-authenticator
      args:
        - "token"
        - "-i"
        - "${var.cluster_name}"
KUBECONFIG
}

output "config-map-aws-auth" {
  value = "${local.config-map-aws-auth}"
  sensitive = true
}

output "kubeconfig" {
  value = "${local.kubeconfig}"
  sensitive = true
}

# The following outputs allow authentication and connectivity to the EKS Cluster
# by using certificate-based authentication.

output "cluster_full_name" {
  value = aws_eks_cluster.xand.name
}

output "cluster_endpoint_uri" {
  description = "The address that can be used to connect to for kubectl"
  value = aws_eks_cluster.xand.endpoint
}

output "nat_ips" {
  description = "The IP address outgoing traffic will be associated with"
  value = [aws_eip.xand_nat.public_ip]
}

output "nat_dns" {
  value = [aws_eip.xand_nat.public_dns]
}
