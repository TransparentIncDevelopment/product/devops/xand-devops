#
# EKS Worker Nodes Resources
#  * IAM role allowing Kubernetes actions to access other AWS services
#  * EC2 Security Group to allow networking traffic
#  * Data source to fetch latest EKS worker AMI
#  * AutoScaling Launch Configuration to configure worker instances
#  * AutoScaling Group to launch worker instances
#

resource "aws_iam_role" "cluster_node" {
  name = "${var.cluster_name}-cluster-node"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": ["sts:AssumeRole"]
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "xand_node_AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.cluster_node.name
}

resource "aws_iam_role_policy_attachment" "xand_node_AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.cluster_node.name
}

resource "aws_iam_role_policy_attachment" "xand_node_AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.cluster_node.name
}

resource "aws_security_group" "xand_node" {
  name        = "${var.cluster_name}-worker-node"
  description = "Security group for all nodes in the cluster"
  vpc_id      = aws_vpc.xand.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = map(
    "Name", "${var.cluster_name}-worker-node",
    "kubernetes.io/cluster/${var.cluster_name}", "shared",
    "kubernetes.io/cluster-autoscaler/enabled", true,
    "propagate_at_launch", true
  )

}

resource "aws_security_group_rule" "xand_node_ingress_self" {
  description              = "Allow node to communicate with each other"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "-1"
  security_group_id        = aws_security_group.xand_node.id
  source_security_group_id = aws_security_group.xand_node.id
  type                     = "ingress"
}

resource "aws_security_group_rule" "xand_node_ingress_cluster" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 1025
  to_port                  = 65535
  protocol                 = "tcp"
  security_group_id        = aws_security_group.xand_node.id
  source_security_group_id = aws_security_group.cluster.id
  type                     = "ingress"
}

resource "aws_security_group_rule" "xand_node_ingress_ssh" {
  description              = "Allow worker nodes ssh login"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  cidr_blocks               = ["0.0.0.0/0"]
  security_group_id        = aws_security_group.xand_node.id
  type                     = "ingress"
}

resource "aws_iam_instance_profile" "node" {
  name = var.cluster_name
  role = aws_iam_role.cluster_node.name
}
