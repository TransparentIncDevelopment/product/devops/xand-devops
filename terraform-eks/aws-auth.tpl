apiVersion: v1
kind: ConfigMap
metadata:
  name: aws-auth
  namespace: kube-system
data:
  mapRoles: |
    # Allows access to machines in the cluster from eks.
    - rolearn: ${cluster_node_arn}
      username: system:node:{{EC2PrivateDNSName}}
      groups:
        - system:bootstrappers
        - system:nodes
    # Allows other users access to kubernetes via the cluster role using AssumeRole.
    - rolearn: ${cluster_arn}
      username: designated_role
      groups:
        - system:masters
