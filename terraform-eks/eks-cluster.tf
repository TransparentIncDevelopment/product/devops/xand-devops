#
# Provider Configuration
#

provider "aws" {
  region  = var.aws_region
  version = "~> 2.59"
}

# Using these data sources allows the configuration to be
# generic for any region.
data "aws_region" "current" {}

data "aws_availability_zones" "available" {}

#
# EKS Cluster Resources
#  * IAM Role to allow EKS service to manage other AWS services
#  * EC2 Security Group to allow networking traffic with EKS cluster
#  * EKS Cluster
#

resource "aws_iam_role" "cluster" {
  name = "${var.cluster_name}-cluster"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": ["sts:AssumeRole"]
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "xand_cluster_AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.cluster.name
}

resource "aws_iam_role_policy_attachment" "xand_cluster_AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.cluster.name
}

resource "aws_security_group" "cluster" {
  name        = "${var.cluster_name}-cluster"
  description = "Cluster communication with worker nodes"
  vpc_id      = aws_vpc.xand.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = map(
    "Name", "${var.cluster_name}-cluster",
    "kubernetes.io/cluster/${var.cluster_name}", "shared",
    "kubernetes.io/cluster-autoscaler/enabled", true,
    "propagate_at_launch", true
  )

}

resource "aws_security_group_rule" "xand_cluster_ingress_node_https" {
  description              = "Allow pods to communicate with the cluster API Server"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.cluster.id
  source_security_group_id = aws_security_group.xand_node.id
  to_port                  = 443
  type                     = "ingress"
}

resource "aws_eks_cluster" "xand" {
  depends_on = [
    aws_vpc.xand,
    aws_internet_gateway.xand,
    aws_nat_gateway.xand,
    aws_subnet.xand_elb,
    aws_subnet.xand_nodes,
    aws_route_table_association.xand_nodes,
    aws_iam_role_policy_attachment.xand_cluster_AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.xand_cluster_AmazonEKSServicePolicy
  ]

  name     = var.cluster_name
  version = var.cluster_version
  role_arn = aws_iam_role.cluster.arn

  vpc_config {
    endpoint_private_access = true
    security_group_ids = [aws_security_group.cluster.id]
    subnet_ids         = concat(aws_subnet.xand_nodes.*.id, aws_subnet.xand_elb.*.id)
  }

}
