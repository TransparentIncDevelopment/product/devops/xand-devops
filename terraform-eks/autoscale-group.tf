data "aws_ami" "eks_node_ami" {
  filter {
    name = "state"
    values = ["available"]
  }

  filter {
    name = "architecture"
    values = ["x86_64"]
  }

  filter {
    name = "name"
    values = ["amazon-eks-node-${var.cluster_version}-*"]
  }

  most_recent = true
  owners = ["amazon"]
}

resource "aws_launch_configuration" "xand" {
  associate_public_ip_address = false
  iam_instance_profile        = aws_iam_instance_profile.node.name
  # EKS Kubernetes Worker AMI with AmazonLinux2 image, (k8s: 1.16.13, docker:18.09.9ce-2.amzn2)
  # Note that the kubelet version must match the cluster version.
  # Note that these amis aare region specific
  image_id                    = data.aws_ami.eks_node_ami.id
  instance_type               = var.aws_instance_type
  name_prefix                 = var.cluster_name
  security_groups             = [aws_security_group.xand_node.id]
  user_data_base64            = base64encode(local.xand-node-userdata)
  key_name                    = var.aws_ssh_keypair_name

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "xand" {
  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_eks_cluster.xand,
    aws_nat_gateway.xand,
    aws_internet_gateway.xand,
    aws_route_table_association.xand_elb,
    aws_route_table_association.xand_nodes,
    aws_iam_role_policy_attachment.xand_node_AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.xand_node_AmazonEKS_CNI_Policy
  ]

  name                 = var.cluster_name
  vpc_zone_identifier  = aws_subnet.xand_nodes.*.id

  desired_capacity     = var.aws_scaling_min_size
  launch_configuration = aws_launch_configuration.xand.name
  max_size             = var.aws_scaling_max_size
  min_size             = var.aws_scaling_min_size


  tag {
    key                 = "Name"
    value               = var.cluster_name
    propagate_at_launch = true
  }

  tag {
    key                 = "kubernetes.io/cluster/${var.cluster_name}"
    value               = "owned"
    propagate_at_launch = true
  }

}

# EKS currently documents this required userdata for EKS worker nodes to
# properly configure Kubernetes applications on the EC2 instance.
# We utilize a Terraform local here to simply Base64 encode this
# information and write it into the AutoScaling Launch Configuration.
# More information: https://docs.aws.amazon.com/eks/latest/userguide/launch-workers.html
locals {
  xand-node-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
/etc/eks/bootstrap.sh '${var.cluster_name}' --container-runtime '${var.container_runtime}' --apiserver-endpoint '${aws_eks_cluster.xand.endpoint}' --b64-cluster-ca '${aws_eks_cluster.xand.certificate_authority.0.data}'
USERDATA
}
