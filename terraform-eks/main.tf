terraform {
  backend "s3" {
    region         = "us-west-2"
    bucket         = "terraform-xand"
    key            = "global/s3/terraform.tfstate"
    dynamodb_table = "terraform-xand"
    encrypt        = true
  }
}
