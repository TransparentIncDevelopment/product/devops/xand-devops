#
# VPC Resources
#  * VPC
#  * Subnets
#  * Internet Gateway
#  * Route Table
#  * NAT Gateway (nat.tf)
#
# Using Public + Private Subnets for public load balancers, but Private Worker Nodes:
# https://aws.amazon.com/blogs/containers/de-mystifying-cluster-networking-for-amazon-eks-worker-nodes/
# https://amazon-eks.s3.us-west-2.amazonaws.com/cloudformation/2020-03-23/amazon-eks-vpc-private-subnets.yaml
#

resource "aws_vpc" "xand" {
  cidr_block = "192.168.0.0/16"

  tags = map(
    "Name", var.cluster_name,
    "kubernetes.io/cluster/${var.cluster_name}", "shared",
    "propagate_at_launch", true
  )
}


### Internet Gateway

resource "aws_internet_gateway" "xand" {
  vpc_id = aws_vpc.xand.id
}

### Public ELB Subnets that is considered public since it is associated with the internet gateway
# From EKS Load Balancer Docs: https://docs.aws.amazon.com/eks/latest/userguide/load-balancing.html
#
# For internal load balancers, your Amazon EKS cluster must be configured to use at least one private
# subnet in your VPC. Kubernetes examines the route table for your subnets to identify whether they
# are public or private. Public subnets have a route directly to the internet using an internet gateway,
# but private subnets do not.

resource "aws_subnet" "xand_elb" {
  count = var.subnet_count

  vpc_id = aws_vpc.xand.id
  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block              = "192.168.1${count.index}.0/24"

  tags = map(
    "Name", "${var.cluster_name}-elb-${count.index}",
    "kubernetes.io/cluster/${var.cluster_name}", "shared",
    "kubernetes.io/role/elb", "1",
    "propagate_at_launch", true
  )
}

resource "aws_route_table" "xand_elb" {
  vpc_id = aws_vpc.xand.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.xand.id
  }

  tags = map(
    "Name", "${var.cluster_name}-elb",
    "kubernetes.io/cluster/${var.cluster_name}", "shared",
    "propagate_at_launch", true
  )
}

resource "aws_route_table_association" "xand_elb" {
  count = var.subnet_count

  route_table_id = aws_route_table.xand_elb.id
  subnet_id      = aws_subnet.xand_elb.*.id[count.index]
}

### Private Subnets for the kubernetes' nodes themselves.
# Route tables for the private subnets will be in the nat.tf file
# where the nat is defined since it does not access the internet
# gateway directly.

resource "aws_subnet" "xand_nodes" {
  count = var.subnet_count

  vpc_id = aws_vpc.xand.id
  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block              = "192.168.${count.index}.0/24"

  tags = map(
    "Name", "${var.cluster_name}-nodes-${count.index}",
    "kubernetes.io/cluster/${var.cluster_name}", "shared",
    "kubernetes.io/role/internal-elb", "1",
    "propagate_at_launch", true
  )
}
