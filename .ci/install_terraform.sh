#!/bin/bash

TERRAFORM_URL="https://releases.hashicorp.com/terraform/0.12.25/terraform_0.12.25_linux_amd64.zip"

apt-get -yqq update
apt-get -yqq install curl zip openssh-client

curl -Lo /tmp/terraform.zip $TERRAFORM_URL

unzip /tmp/terraform.zip
chmod +x terraform
mv terraform /usr/local/bin/
