#!/bin/bash

set -o errexit # abort on nonzero exit status
set -o nounset # abort on unbound variable
set -o pipefail # abort if any element of a pipeline fails

#   Use find, sed, sort and uniq to narrow down all directories with .tf files in them, and put them in an array

mapfile -d $'\0' TerraformDirs < <(find . -type f -name '*.tf*' | sed -r 's|/[^/]+$||' |sort |uniq)

#   iterate through said array

for val in ${TerraformDirs[@]}; do
  echo $val
  pushd $val
  pwd

  #   initalize and validate Terraform

  terraform init -input=false -backend=false
  terraform validate

  popd

done
